from django.contrib.auth import authenticate
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import User


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "email", "first_name", "last_name", "password"]

    def create(self, validated_data):
        new_user = User.objects.create_user(**validated_data)
        return new_user


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(min_length=8, max_length=16, required=True)

    def validate(self, data):
        request = self.context.get("request")

        self.user_cache = authenticate(request, username=data["email"], password=data["password"])
        if self.user_cache is None:
            raise self.get_invalid_login_error()
        else:
            self.confirm_login_allowed(self.user_cache)

        return data

    def get_invalid_login_error(self):
        return serializers.ValidationError("Please enter a correct email address and password.")

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise serializers.ValidationError("This account is inactive.")


class ResetPasswordRequestSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class ResetPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(required=True, min_length=8, max_length=16)
    confirm_password = serializers.CharField(required=True, min_length=8, max_length=16)

    def validate(self, attrs):
        if attrs["password"] != attrs["confirm_password"]:
            raise ValidationError("Password do not match!")

        return attrs
