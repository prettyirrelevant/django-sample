from django.urls import path

from . import views

urlpatterns = [
    path("get_csrf", views.get_csrf, name="get-csrf"),
    path("registration", views.UserRegistrationView.as_view(), name="registration"),
    path("login", views.UserLoginView.as_view(), name="login"),
    path(
        "activate_account/<uid>/<token>",
        views.AccountVerificationView.as_view(),
        name="activate-account",
    ),
    path(
        "resend_activation_mail",
        views.ResendVerificationEmailView.as_view(),
        name="resend-activation-mail",
    ),
    path(
        "reset_password_request",
        views.ResetPasswordRequestView.as_view(),
        name="reset-password-request",
    ),
    path("reset_password/<uid>/<token>", views.ResetPasswordView.as_view(), name="reset-password"),
    path("logout", views.UserLogoutView.as_view(), name="logout"),
]
