from django.contrib.auth.tokens import PasswordResetTokenGenerator

from .models import User


class UserActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user: User, timestamp: int) -> str:
        return f"{user.pk}{user.is_email_verified}{timestamp}"


user_activation_token = UserActivationTokenGenerator()

reset_password_token = PasswordResetTokenGenerator()
