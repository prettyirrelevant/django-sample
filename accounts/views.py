from django.contrib.auth import login, logout
from django.core.exceptions import ValidationError
from django.middleware.csrf import get_token
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import User
from .serializers import (
    ResetPasswordRequestSerializer,
    ResetPasswordSerializer,
    UserLoginSerializer,
    UserRegistrationSerializer,
)
from .tasks import send_async_password_reset_mail, send_async_verification_mail
from .tokens import reset_password_token, user_activation_token
from .utils import sensitive_post_parameters


class UserRegistrationView(generics.CreateAPIView):
    """
    Endpoint for user registration/creation.
    """

    serializer_class = UserRegistrationSerializer
    queryset = User

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        user = User.objects.get(email=serializer.data["email"])
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        token = user_activation_token.make_token(user)

        send_async_verification_mail(user.first_name, user.last_name, user.email, token, uid)

        return Response(
            {
                "status": "success",
                "detail": "Account created successfully. Please check email for verification!",
            },
            status=status.HTTP_201_CREATED,
            headers=headers,
        )


class UserLoginView(generics.GenericAPIView):
    """
    Endpoint for user login.
    """

    queryset = User
    serializer_class = UserLoginSerializer

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def post(self, request, format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        self.login(request, serializer.data)

        return Response(
            {
                "status": "success",
                "detail": "Login successful!",
            },
            status=status.HTTP_200_OK,
        )

    def login(self, request, data):
        queryset: User = self.get_queryset()
        user = queryset.objects.get(email=data["email"])

        login(request, user)


class AccountVerificationView(generics.GenericAPIView):
    """
    Endpoint to verify user account.
    """

    def post(self, request, *args, **kwargs):
        try:
            uid = force_str(urlsafe_base64_decode(kwargs.get("uid")))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist, ValidationError):
            user = None

        if user is not None and user_activation_token.check_token(user, kwargs.get("token")):
            user.is_email_verified = True
            user.email_verified_at = timezone.now()
            user.save()

            return Response(
                {"status": "success", "detail": "You have successfully activated your account!"},
                status=status.HTTP_200_OK,
            )

        else:
            return Response(
                {"status": "error", "detail": "The token has expired, already used or invalid!"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class ResetPasswordRequestView(generics.GenericAPIView):
    """
    Endpoint to request for password reset.
    """

    serializer_class = ResetPasswordRequestSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            user = User.objects.get(email=serializer.data["email"])

            uid = urlsafe_base64_encode(force_bytes(user.pk))
            token = reset_password_token.make_token(user)

            send_async_password_reset_mail(user.first_name, user.last_name, user.email, token, uid)
        except User.DoesNotExist:
            pass

        return Response(
            {
                "status": "success",
                "detail": "Check your email for the instructions to reset your password!",
            },
            status=status.HTTP_200_OK,
        )


class ResetPasswordView(generics.GenericAPIView):
    """
    Endpoint to reset password.
    """

    serializer_class = ResetPasswordSerializer

    @method_decorator(sensitive_post_parameters())
    @method_decorator(never_cache)
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            uid = force_str(urlsafe_base64_decode(kwargs.get("uid")))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist, ValidationError):
            user = None

        if user is not None and reset_password_token.check_token(user, kwargs.get("token")):
            user.set_password(serializer.data["password"])
            user.save()

            logout(request)

            return Response(
                {"status": "success", "detail": "Password reset successful!"},
                status=status.HTTP_200_OK,
            )
        else:
            return Response(
                {"status": "error", "detail": "The token has expired, already used or invalid!"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class ResendVerificationEmailView(generics.GenericAPIView):
    """
    Endpoint for resending verification email to user.
    """

    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        uid = urlsafe_base64_encode(force_bytes(request.user.pk))
        token = user_activation_token.make_token(request.user)

        send_async_verification_mail(
            request.user.first_name, request.user.last_name, request.user.email, token, uid
        )

        return Response(
            {"status": "success", "detail": "A new activation email has been sent!"},
            status=status.HTTP_200_OK,
        )


class UserLogoutView(generics.GenericAPIView):
    """
    Endpoint to log out user.
    """

    @method_decorator(never_cache)
    def delete(self, request, *args, **kwargs):
        logout(request)
        return Response(
            {"status": "success", "detail": "User logged out successfully!"},
            status=status.HTTP_200_OK,
        )


@api_view(["GET"])
def get_csrf(request):
    response = Response(
        {"status": "success", "detail": "CSRF cookie set"}, status=status.HTTP_200_OK
    )
    response["X-CSRFToken"] = get_token(request)
    return response
