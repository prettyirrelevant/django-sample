import logging
from datetime import timedelta

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone
from huey import crontab
from huey.contrib.djhuey import db_periodic_task, task

from .models import User

logger = logging.getLogger("huey")


@task()
def send_async_verification_mail(first_name, last_name, to, token, uid):
    logger.info(f"Sending verification email to: {to}")

    subject = "[App.dev] Please Verify Your Account"
    html_content = render_to_string(
        "accounts/email_verification.html",
        {"first_name": first_name, "last_name": last_name, "token": token, "uid": uid},
    )

    mail = EmailMultiAlternatives(subject, to=[to])
    mail.attach_alternative(html_content, "text/html")
    mail.send()

    logger.info(f"Verification email sent to: {to}")


@task()
def send_async_password_reset_mail(first_name, last_name, to, token, uid):
    logger.info(f"Sending password reset email to: {to}")

    subject = "[App.dev] Reset Your Password"
    html_content = render_to_string(
        "accounts/reset_password.html",
        {"first_name": first_name, "last_name": last_name, "token": token, "uid": uid},
    )

    mail = EmailMultiAlternatives(subject, to=[to])
    mail.attach_alternative(html_content, "text/html")
    mail.send()

    logger.info(f"Password reset email sent to: {to}")


@db_periodic_task(crontab(hour="*/6"))
def delete_not_verified_accounts():
    now = timezone.now()
    a_day_ago = now - timedelta(days=1)

    users = User.objects.filter(is_email_verified=False, date_joined__lte=a_day_ago)

    logger.info(f"Attempting to delete a total of {users.count()} users -> {users}")
    users.delete()

    logger.info("Deletion Complete!")
