import functools

from rest_framework.request import Request


def sensitive_post_parameters(*parameters):
    """
    `sensitive_post_parameters` adapted for DRF.
    """
    if len(parameters) == 1 and callable(parameters[0]):
        raise TypeError(
            "sensitive_post_parameters() must be called to use it as a "
            "decorator, e.g., use @sensitive_post_parameters(), not "
            "@sensitive_post_parameters."
        )

    def decorator(view):
        @functools.wraps(view)
        def sensitive_post_parameters_wrapper(request, *args, **kwargs):
            if isinstance(request, Request):  # handling DRF
                request._request.sensitive_post_parameters = parameters or "__ALL__"
            else:
                if parameters:
                    request.sensitive_post_parameters = parameters
                else:
                    request.sensitive_post_parameters = "__ALL__"
            return view(request, *args, **kwargs)

        return sensitive_post_parameters_wrapper

    return decorator
